package ru.tsc.kirillov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.kirillov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Insert("INSERT INTO task (id, created, name, description, status, user_id, project_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull Task model);

    @Insert("INSERT INTO task (id, created, name, description, status, user_id, project_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<Task> models);

    @Delete("DELETE FROM task")
    void clear();

    @Delete("DELETE FROM task WHERE user_id = #{userId}")
    void clearUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAll();

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Task> findAllComparator(SelectStatementProvider selectStatementProvider);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Task> findAllComparatorUserId(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM task WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM task WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task LIMIT #{index}, 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneByIndex(@Param("index") @NotNull Integer index);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task "
            + "WHERE user_id = #{userId} LIMIT #{index}, 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneByIndexUserId(@Param("userId") @Nullable String userId, @Param("index") @Nullable Integer index);

    @Delete("DELETE FROM task WHERE id = #{id}")
    int remove(@NotNull Task model);

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Task> collection);

    @Update("UPDATE task SET created = #{created}, name = #{name}, description = #{description}, "
            + "status = #{status}, project_id = #{projectId}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    int update(@NotNull Task model);
    
    @Select("SELECT count(1) FROM task")
    long count();

    @Select("SELECT count(1) FROM task WHERE user_id = #{userId}")
    long countUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM task "
            + "WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@Param("userId") String userId, @Param("projectId") String projectId);

    @Delete("DELETE FROM task WHERE user_id = #{userId} AND project_id in (#{projectId})")
    void removeAllByProjectList(@Param("userId") @Nullable String userId, @Param("projects") @Nullable String[] projects);

}
