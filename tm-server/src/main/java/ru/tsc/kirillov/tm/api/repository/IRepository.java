package ru.tsc.kirillov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    int removeById(@NotNull String id);

    int removeByIndex(@NotNull Integer index);

    int update(@NotNull M model);

    long count();

}
