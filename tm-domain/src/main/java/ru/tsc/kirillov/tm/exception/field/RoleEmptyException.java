package ru.tsc.kirillov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Ошибка! Роль не задана.");
    }

}
