package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectCreateRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание нового проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        @NotNull final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        @NotNull final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final ProjectCreateRequest request =
                new ProjectCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getProjectEndpoint().createProject(request);
    }

}
