package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить все задачи.";
    }

    @Override
    public void execute() {
        System.out.println("[Очистка списка задач]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

}
